
public abstract class Modele {
	
	private String name;
	private Repertoire parent;
	
	public Modele(String name, Repertoire parent) {
		this.name = name;
		this.parent = parent;
	}
	
	public Repertoire getParent() {
		return parent;
	}

	public void setParent(Repertoire parent) {
		this.parent = parent;
	}
	
	public String getName() {
		return this.name; 
	}
	
	abstract public int getTaille();
	
	public boolean compare(Modele f) {
		// TODO Auto-generated method stub
		if(f.getName().equals(this.name) && f.getClass().getName().equals(this.getClass().getName())) return true;
		return false;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return this.getName();
	}
	
	public boolean VerifRepertoire() {
		return "Repertoire".equals(this.getClass().getName());
	}
}
