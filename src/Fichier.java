
public class Fichier extends Modele {
	
	private int taille;

	//constructeur
	public Fichier(String name, Repertoire parent, int taille) {
		super(name, parent);
		this.taille = taille;
	}

	public int getTaille() {
		return taille;
	}

	public String getName() {
		return super.getName();
	}
	
	public void setTaille(int taille) {
		this.taille = taille;
	}
	
	public void setName(String name) {
		super.setName(name);
	}
	
	@Override
	public Repertoire getParent() {
		return super.getParent();
	}
	
}
