import java.util.ArrayList;

public class Repertoire extends Modele {
	
	private ArrayList<Modele> ListeFichierRep;
	
	//constructeur
	public Repertoire(String name, Repertoire parent) {
		super(name, parent);
		this.ListeFichierRep = new ArrayList<Modele>();
	}
	
	//Construction pour la création d'une racine
	public Repertoire(String name) {
		super(name, null);
		this.ListeFichierRep = new ArrayList<Modele>();
	}

	@Override
	public int getTaille() {
		int taille = 0;
		for (Modele fichRep : this.ListeFichierRep) {
			taille = taille + fichRep.getTaille();
		}
		return taille;
	}
	
	//vérifie si un ficher ou repertoire est contenu dans le repertoire pere
	public boolean contenu(Modele modele)
	{
		for (Modele fichierdosier : this.ListeFichierRep) {
			if (modele.compare(fichierdosier)) {
				return true;
			}		
		}
		return false;
	}
	
	//ajouter un modele dans un repertoire
	public void ajout(Modele modele)
	{	
		if (modele.getClass().getName().equals("Repertoire") && this.verifParent((Repertoire) modele)) {
			System.err.println("Ce repertoire existe déjà dans l'arborescence parentale");
		}
		else {
			if (!this.contenu(modele) && !this.compare(modele) && modele.getParent() != null) {
				this.ListeFichierRep.add(modele);
			}
			else {
				System.err.println("\nERROR : Un problème a survenu lors de l'ajout");
			}
		}
	}

	//Fonction pour vérifier si un répertoire est un parent de lui même
	public boolean verifParent(Repertoire dir) {
		
		if(this.contenu(dir) || this.compare(dir)) return true;
		
		Repertoire par = (Repertoire) dir.getParent();
		
		while(par != null) {	
			if(par.contenu(dir) || this.compare(dir)) return true;
			par = par.getParent();
		}
		
		return false;
	}

	@Override
	public Repertoire getParent() {
		return super.getParent();
	}
	
}
