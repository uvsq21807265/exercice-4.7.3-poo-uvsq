
public class Principale {

	public static void main(String[] args) {
		
		//creation de la racine
		Repertoire racine = new Repertoire ("/", null);
		
		//répértoire pour utilisateurs
		Repertoire bin = new Repertoire ("/bin", racine);
		//ajout du répertoire utilisateur à la racine
		racine.ajout(bin);
		
		//creation du repertoire home
		Repertoire home = new Repertoire ("/home", racine);
		//ajout du répertoire home à la racine
		racine.ajout(home);
		
		//répértoire pour dev
		Repertoire dev = new Repertoire ("/dev", racine);
		//ajout du  utilisateur à la dev
		racine.ajout(dev);
		
		//répértoire pour lib
		Repertoire lib = new Repertoire ("/lib", racine);
		//ajout du répertoire utilisateur à la racine
		racine.ajout(lib);
		
		//création du fichier1
		Fichier fich1 = new Fichier("fich1", home, 30);
		//ajout à la racine:
		racine.ajout(fich1);
		
		//création du dossier etudiant1 dans /home:
		Repertoire etudiant1 = new Repertoire ("/home/etudiant1", home);
		//ajout de etudiant1 à /home
		home.ajout(etudiant1);
		
		//création du dossier etudiant3 dans /home:
		Repertoire etudiant3 = new Repertoire ("/home/etudiant3", etudiant1);
		//ajout de etudiant3 à /etudiant1
		etudiant1.ajout(etudiant3);
		
		//création du dossier etudiant4 dans /home:
		Repertoire etudiant4 = new Repertoire ("/home/etudiant4", etudiant3);
		//ajout de etudiant4 à /etudiant3
		etudiant3.ajout(etudiant4);
		
		//création du dossier etudiant3 dans /home:
		Repertoire etudiant5 = new Repertoire ("/home/etudiant5", etudiant4);
		//ajout de etudiant4 à /home
		etudiant4.ajout(etudiant5);
		
		//création du dossier etudiant dans /home:
		Repertoire etudiant2 = new Repertoire ("/home/etudiant2", home);
		//ajout de etudiant2 à /home
		home.ajout(etudiant2);
		
		//création du fichier2
		Fichier fich2 = new Fichier("fich2", etudiant1, 40);
		//ajout de fich2 à etudiant1:
		etudiant1.ajout(fich2);
	 	
		//création du fichier3
		Fichier fich3 = new Fichier("fich3", etudiant2, 50);
		//ajout de fich3 à etudiant2:
		etudiant2.ajout(fich3);
		
		//création du fichier4
		Fichier fich4 = new Fichier("fich4", etudiant1, 60);
		//ajout de fich4 à etudiant1:
		etudiant1.ajout(fich4);
		
		//création du fichier5
		Fichier fich5 = new Fichier("fich5", etudiant2, 70);
		//ajout de fich5 à etudiant2:
		etudiant2.ajout(fich5);
		
		int t;
		t = racine.getTaille();
		System.out.println("Taile de home : " + t);
		
		//System.out.println(home.getClass());
		
		//System.out.println("here"+etudiant1.getClass().getName());
		
		//test verifRepetoire()
		/*if (etudiant1.VerifRepertoire()) {
			System.out.println(etudiant1.getName()+" est un répertoire");
		}
		else {
			System.out.println(etudiant1.getName()+" n'est pas un répertoire");
		}*/
		
		//test compare
		if(etudiant1.compare(etudiant2)) {
			System.out.println("Les répertoires sont égaux");
		}
		else {
			System.out.println("Les répertoires ne sont pas égaux");
		}
		
		//test ajout un répertoire dans lui même
		//etudiant1.ajout(etudiant1);
		
		//test compare
		if(etudiant1.verifParent(home)) {
			System.out.println("Home est un parent de étudiant1");
		}
		else {
			System.out.println("Pas un parent");
		}
		
		/*//test ajout un répertoire descendant de lui même
		etudiant1.ajout(home);*/
		
		etudiant5.ajout(home);
	}
}
